# datascriptor-api

The back-end API for the Datascriptor application

## Installation

`yarn install` (or `npm install`)

## Start the application

`yarn start` (or `npm start`)

## Test the application

`yarn test` (or `npm test`)