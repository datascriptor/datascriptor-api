/**
 * This project structure was inspired by https://github.com/focusaurus/express_code_structure
 */
const express = require('express');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.set('useFindAndModify', false);


const config = require('./config');

const app = express();
// const database = 'datascriptor';

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// Wire/Load all the routes here
app.use('/api', require('./app/study/study-router'));
app.use('/api', require('./app/studyDesign/studyDesign-router'));

// app.set('useStudyExpanded', false);

const uri = `mongodb://${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.database}`;
console.log(`MongoDB connection is ${uri}`);

mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Connected to MongoDB...');
    mongoose.connection.on('error', err => {
        console.log(`Something shucked up while connecting to MongoDB: ${err}`);
    });
}).catch(err => {
    console.log(`MongoDB connection failed: ${err}`);
    process.exit(-1);
});

module.exports = exports = app;