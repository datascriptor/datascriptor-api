const env = process.env.NODE_ENV || 'development';
const config = require(`./${env}`);

const PRODUCTION = process.env.NODE_ENV === 'production';

if (PRODUCTION) {
    // for example
    config.express.ip = '0.0.0.0';
}

module.exports = config;