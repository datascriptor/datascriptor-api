const { expect } = require('chai');
const { isEmpty } = require('lodash');
const request = require('supertest');
const app = require('../../server');
const testFullFactorialPayload = {
    name: 'Test FF Study Design',
    type: 'fullFactorial',
    description: '`This is meant to be a full factorial design for testing purposes',
    elementParams: {
        agents: ['honey', 'sugar', 'molasses'],
        intensities: [0.1, 1.0, 10.0],
        durations: [7, 14],
        intensityUnit: 'cl/Kg',
        durationUnit: 'day'
    },
    subjectType: 'Human',
    size: 8
};

describe('studyDesignRouter', function() {

    describe('POST /studyDesign/generate', function() {

        it('should generate a full factorial design from element parameters', function(done) {
            request(app)
                .post('/api/studyDesign/generate')
                .send(testFullFactorialPayload)
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    const payload = res.body;
                    const { name, subjectType, size,  elementParams: {
                        agents, intensities, durations
                    } } = testFullFactorialPayload;
                    expect(payload.name).to.equal(name);
                    expect(payload).to.have.property('arms');
                    expect(payload).to.have.property('elements');
                    expect(payload).to.have.property('events');
                    expect(payload.arms).to.have.length(
                        agents.length * intensities.length * durations.length
                    );
                    for (const arm of payload.arms) {
                        expect(arm.subjectType).to.equal(subjectType);
                        expect(arm.size).to.equal(size);
                        expect(arm).to.have.property('epochs');
                    }
                    done();
                })
                .catch(done);
        });
    });

});

