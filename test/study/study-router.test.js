const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../server');
const { Study } = require('../../app/study/study-model');
const mongoose = require('mongoose');
const testStudy = require('../fixtures/lite/study');

Study.counterReset('mongo_id', err => {
    if (err) {
        console.log(`Error thrown while resetting counter: ${err}`);
    }
});

describe('studyRouter', function() {

    before(async function() {
        const res = await Study.remove({});
        console.log(`study-router.test.js - setUp method: #${res.deletedCount} items were removed from the collection`)
    });

    describe('POST /api/study', function() {
        it('should create a new instance of a study', function(done) {
            const newId = 1;
            let _id;
            request(app)
                .post('/api/study')
                .send(testStudy)
                .expect('Content-Type', /json/)
                .expect(201)
                .expect('Location', `/api/study/${newId}`)
                .expect(res => {
                    console.log(res.body);
                    res.body._id = 0;
                    console.log(`_id = ${_id}`);
                    console.log(`res.body._id = ${res.body._id}`);
                })
                .then(res => {
                    const payload = res.body;
                    expect(payload.id).to.equal(testStudy.id);
                    expect(payload.mongo_id).to.equal(1);
                    expect(payload.name).to.equal(testStudy.name);
                    expect(payload).to.have.nested.property('design.arms');
                    for (const [index, arm] of payload.design.arms.entries()) {
                        expect(arm.name).to.equal(testStudy.design.arms[index].name);
                        expect(arm.subjectType).to.equal(testStudy.design.arms[index].subjectType);
                        expect(arm.size).to.equal(testStudy.design.arms[index].size);
                    }
                    done();
                })
                .catch(done);
        });
    });

    describe('GET /api/study', function() {
        it('should return a list with one item', function(done) {
            request(app)
                .get('/api/study')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    const payload = res.body;
                    expect(payload).to.be.an.instanceOf(Array);
                    expect(payload).to.have.lengthOf(1);
                    expect(payload[0].id).to.equal(testStudy.id);
                    expect(payload[0].mongo_id).to.equal(1);
                    expect(payload[0].name).to.equal(testStudy.name);
                    done();
                })
                .catch(done);
        });
    });

    describe('POST /api/study/convert', function() {
        it('should return a json with the text describing the dataset', function(done) {
            request(app)
                .post('/api/study/convert')
                .send(testStudy)
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    const payload = res.body;
                    expect(payload).to.have.property('arms');
                    expect(payload.arms).to.have.property('control');
                    expect(payload.arms).to.have.property('treatment');
                    done();
                })
                .catch(done);
        });
    });

    after(async function() {
        const res = await Study.remove({});
        console.log(`study-router.test.js - tearDown method: #${res.deletedCount} items were removed from the collection`);
    });

});