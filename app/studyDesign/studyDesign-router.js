const express = require('express');
const router = express.Router();
const { lite: { StudyDesignFactory } } = require('datascriptor-core');


const designGenerators = {

    fullFactorial: function generateFullFactorial(payload) {
        return StudyDesignFactory.computeFullFactorialDesign(payload);
    }

};

function generate(req, res) {
    try {
        const { type, ...reqPayload } = req.body;
        const studyDesign = designGenerators[type](reqPayload);
        return res.status(200).json(studyDesign);
    }
    catch (err) {
        return res.status(500).json({
            message: err.message  
        });
    }
}

router.post('/studyDesign/generate', generate);

module.exports = exports = router;