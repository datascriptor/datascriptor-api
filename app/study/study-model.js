const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const studySchema = new Schema({

    id: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    design: {
        type: {
            type: String
        },
        name: {
            type: String,
            required: false
        },
        description: {
            type: String
        },
        elements: [{
            id: String,
            name: String,
            duration: Number,
            durationUnit: Schema.Types.Mixed,
            agent: Schema.Types.Mixed,
            intensity: Number,
            intensityUnit: Schema.Types.Mixed
        }],
        events: [{
            id: String,
            action: String,
            input: Schema.Types.Mixed,
            output: Schema.Types.Mixed,
            outputSize: Number,
            parameters: Schema.Types.Mixed
        }],
        arms: [{
            id: String,
            name: {
                type: String,
                required: true
            },
            subjectType: Schema.Types.Mixed,
            size: Number,
            epochs: [{
                elements: [{
                    type: String
                }],
                events: [{
                    type: String
                }]
            }]
        }]
    },
    dataset: Schema.Types.Mixed

});

studySchema.plugin(AutoIncrement, {
    inc_field: 'mongo_id'
});

const Study = mongoose.model('Study', studySchema);

/**
 * @function
 * @name create
 * @param {Object} study - object to be created
 * @returns Object - the created study
 */
async function create(study) {
    const createdStudy = await Study.create(study);
    return createdStudy;
}

async function retrieveOne(name) {
    const study = await Study.findOne({ name });
    return study;
}

async function retrieveAll(criteria =  null) {
    const studies = await Study.find(criteria);
    return studies;
}

function update() {}

function destroy() {}

module.exports = exports = {
    Study,
    create,
    retrieveOne,
    retrieveAll,
    update,
    destroy
};