const express = require('express');
const fs = require('fs');
const formidable = require('formidable');
const router = express.Router();
const StudyModel = require('./study-model');
// const ExpandedStudyModel = require('./expanded-study-model');
const { lite } = require('datascriptor-core');

const LOCATION_HEADER = '/api/study';

async function findAll(req, res) {
    try {
        const records = await StudyModel.retrieveAll();
        return res.json(records);
    } 
    catch (err) {
        return res.status(500).json({
            message: err.message  
        });
    }
}

async function create(req, res) {
    const json = req.body;
    try {
        console.log(`Creating a new study: ${json}`);
        const created = await StudyModel.create(json);
        console.log(`Created a new study: ${created}`);       
        return res.status(201)
            .location(`${LOCATION_HEADER}/${created.mongo_id}`) // let's use mongo_id for now
            .json(created);
    }
    catch (err) {
        return res.status(500).json({
            message: err.message  
        });
    }
}

function convert(req, res) {
    try {
        console.log(`study-model.convert() - request payload: ${JSON.stringify(req.body)}`);
        const study = lite.Study.fromJSON(req.body);
        console.log(`study-model.convert() - study to convert: ${JSON.stringify(study)}`);
        const textFactory = new lite.StudyTextFactory(study);
        const resPayload = {
            arms: textFactory.generateArmsReport(),
            elements: textFactory.generateElementsReport(),
            events: textFactory.generateEventsReport()
        };
        return res.status(200).json(resPayload);
    }
    catch (err) {
        console.log(err);
        return res.status(500).json({
            message: err.message  
        });
    }
}

function uploadAndConvert(req, res) {
    try {
        const form = new formidable.IncomingForm();
        form.parse(req, (_, fields, files) => {
            console.log('\n-----------');
            console.log('Fields', fields);
            console.log('Received:', files);
            const [file, ...rest] = Object.values(files);
            const reqPayload = JSON.parse(fs.readFileSync(file.path, 'utf8'));
            const study = lite.Study.fromJSON(reqPayload);
            const textFactory = new lite.StudyTextFactory(study);
            const studyReport = {
                arms: textFactory.generateArmsReport(),
                events: textFactory.generateEventsReport(),
                elements: textFactory.generateElementsReport()
            };
            const resPayload = {
                studyData: reqPayload,
                studyReport
            };
            res.status(200).json(resPayload);
        });
    }
    catch (err) {
        console.log(err);
        return res.status(500).json({
            message: err.message  
        });
    }
}

function createAndConvert(req, res) {}

function getAndConvert(req, res) {}

router.get('/study', findAll);
router.post('/study', create);
router.post('/study/convert', convert);
router.post('/study/uploadAndConvert', uploadAndConvert);

module.exports = exports = router;